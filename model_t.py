from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from metrics_module import collect_metrics

def train():
    iris = datasets.load_iris()

    data = pd.DataFrame({
        'sepal length': iris.data[:, 0],
        'sepal width': iris.data[:, 1],
        'petal length': iris.data[:, 2],
        'petal width': iris.data[:, 3],
        'species': iris.target
    })
    data.head()


    X = data[['sepal length', 'sepal width', 'petal length', 'petal width']]
    y = data['species']


    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=85)


    clf = RandomForestClassifier(n_estimators=100)
    clf.fit(X_train, y_train)

    return clf


def train_wth_metrics():
    clf = train()
    collect_metrics(clf)
    save_model("../clf_model.pkl", clf)


def save_model(pkl_filename, model):
    with open(pkl_filename, 'wb') as file:
        pickle.dump(model, file)


if __name__ == "__main__":
    clf = train()
    train_wth_metrics()
